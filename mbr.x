ENTRY(start)

MEMORY {
    /* 1.44 MiB */
    image : ORIGIN = 0, LENGTH = 1474560
}

SECTIONS {
    /* MBR loads in memory at 0x7C00 but comes at the beginning of the disk. */
    .mbr 0x7c00 : {
        mbr.o(.text)
    } AT>image

    .picture 0x8000 : {
        _mbr_sector_lma = .;
        picture.o(.data)
    } AT>image

    /DISCARD/ : {
        *(.dynamic .eh_frame)
    }
}
        
