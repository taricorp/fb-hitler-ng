# Making a PDF becomes slightly harder when we need to be able to read the
# sectors containing our picture but keep the PDF header in the first kilobyte.
# For now don't do it.
a.bin: a.out
	objcopy -O binary a.out a.bin
	truncate -s 1474560 a.bin

a.out: mbr.o picture.o
	ld -Tmbr.x mbr.o picture.o

%.o : %.S
	as -o $@ $<

about.pdf: about.tex
	xelatex -interaction nonstopmode about.tex

.PHONY: run
run: a.bin
	bochs -rc bx_enh_dbg.rc -q -f bochs.conf

.PHONY: clean
clean:
	rm -f floppy.img hitler.o hitler.bin

.PHONY: examine
examine: a.out
	objdump -mi8086 -d a.out | less
