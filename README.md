A version of XKCD's Windows 7.

![Adolf hitler with flashing eyes](demo.gif)

This generates a bootable floppy image that can be run in QEMU:

    qemu-system-i386 -fda a.bin -monitor stdio

Typing `sendkey ctrl-alt-delete` in the terminal will then flash the eyes.

Read more: https://www.taricorp.net/projects/fb-hitler/
