I want to make a MBR/PE polyglot that is bootable with both EFI and legacy
BIOS.

PE mostly just requires that we start with 'MZ' and have the 32-bit file offset
of the PE header at offset 3C. Bonus points if we maintain the file as a valid
DOS executable, so the same program can be run in DOS, EFI or booted directly.

xref http://www.openrce.org/reference_library/files/reference/PE%20Format.pdf
and http://www.tavi.co.uk/phobos/exeformat.html

One page is 512 bytes and one paragraph is 16 bytes.

4d 5a       # Required
00 00       # Number of bytes used in the last page (0 means full page)
00 00       # Number of pages to load
00 00       # Number of relocation entries following header
00 00       # Number of paragraphs in this header
00 00       # Number of additional paragraphs of memory (.bss size)
00 00       # Max size of extra memory to reserve
00 00       # Initial SS, relative to program load segment
00 00       # Initial SP
00 00       # Checksum. May be 0.
00 00       # Initial IP
00 00       # Initial CS, relative to program load segment
00 00       # File offset of first relocation
00 00       # Overlay number. Usually 0.

Double-bonus: make the file a valid PDF too.
This is actually very easy, since we just need to put a valid PDF signature
in the first 1024 bytes of the file. So we can just append a PDF to the boot
sector.

https://docs.google.com/presentation/d/1WLZR06LPM1HQJln-xCMFGOMjmM-xULwIPh4uhM5-IX0/edit?usp=sharing
https://docs.google.com/presentation/d/1pXy9_kymhkkMticVxEl5AzAgYxr-Rl5lXWMg-ZsgmQ4/edit?usp=sharing

# UEFI version

So, UEFI. It's a whole pile of things that notionally makes firmware easier.
It's a totally free spec, so that's a plus.

Tianocore is the open-source bits of the implementation released by Intel
to go with the spec. This is a huge bonus over the accreted de facto standards
of BIOS.

Debugging/testing might be a little harder. But EDK has us covered with
simulators that can run on a number of platforms, like windows (NT32) or a
virtual machine (OVMF; Qemu or KVM).

There's also a GNU implementation of EFI tools, but EDK is portable and more
official so even if it's larger and rather more complex I think it's a better
choice. So we'll use the latest UDK release (stable release of EDK II tools)
to develop against.

Downloaded UDK2017, unpacked and built the tools

```
make -C BaseTools
. ./edksetup.sh
```

Then configure to compile the MdeModulePkg (bare-bones package for portable
application development) for the desired CPU and with the desired compiler,
editing Conf/target.txt
