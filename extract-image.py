import mmap
import struct

f = open('hitler-vga.bmp', 'rb')
mm = mmap.mmap(f.fileno(), 0, prot=mmap.PROT_READ)
def int16(bs):
    return struct.unpack('<h', bs)[0]
def int32(bs):
    return struct.unpack('<i', bs)[0]

def require(have, expect, message):
    if have != expect:
        raise ValueError("{}: expected {}, got {}".format(message, expect, have))

require(mm[:2], b'BM', "Not a Windows bitmap")
require(int32(mm[14:18]), 108, "Header not BITMAPV4HEADER")
require(int32(mm[18:22]), 320, "not 320 pixels wide")
require(int32(mm[22:26]), 200, "not 200 pixels tall")
require(int16(mm[28:30]), 8, "not 8 bpp")
require(int32(mm[30:34]), 0, "not uncompressed")

data_offset = int32(mm[10:14])
row_starts = reversed(range(data_offset, len(mm), 320))

for (i, row_start) in enumerate(row_starts):
    values = mm[row_start:row_start+320]
    print('# Row {}'.format(i))
    print('.byte', ','.join('{:#04x}'.format(x) for x in values))
